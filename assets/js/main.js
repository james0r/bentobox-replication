import '../scss/style.scss'
import './vert-carousel.js'
import vertCarousel from './vert-carousel.js';
import horizCarousel from './horiz-carousel.js';

// Detect scroll and apply fixed navbar class 
// when scrolling away from the top of screen

window.addEventListener('scroll', function () {
    let scrollPosition = window.scrollY;
    let navbar = document.getElementById('navbar');


    if (scrollPosition > 0) {
        navbar.classList.add('navbar-fixed');
    } else {
        navbar.classList.remove('navbar-fixed');
    }
})

document.getElementById('navbar-toggler').addEventListener('click', function () {
    //hide normal navbar

    let navbar = document.getElementById('navbar');
    let navbarOverlay = document.getElementById('navbar-overlay');

    navbar.classList.add('hidden');
    navbarOverlay.classList.remove('hidden')
    navbarOverlay.classList.add('navbar-overlay-shown');
    document.getElementsByTagName('body')[0].style.overflow = "hidden";
    setTimeout(function () {
        console.log("performed scroll to top");
        window.scrollTo(0, 0);
    }, 1100);

})

document.getElementById('navbar-overlay-toggler').addEventListener('click', function () {
    //hide normal navbar

    let navbar = document.getElementById('navbar');
    let navbarOverlay = document.getElementById('navbar-overlay');

    navbar.classList.remove('hidden');
    navbarOverlay.classList.add('hidden');
    navbarOverlay.classList.remove('navbar-overlay-shown');
    document.getElementsByTagName('body')[0].style.overflow = "auto";
})

//close overlay if screen width reaches 991px while overlay is showing
function myFunction(x) {
    if (x.matches) { // If media query matches
        let navbar = document.getElementById('navbar');
        let navbarOverlay = document.getElementById('navbar-overlay');

        navbar.classList.remove('hidden');
        navbarOverlay.classList.add('hidden');
        navbarOverlay.classList.remove('navbar-overlay-shown');
        document.getElementsByTagName('body')[0].style.overflow = "auto";
    }
}

var x = window.matchMedia("(min-width: 991px)")
myFunction(x) // Call listener function at run time
x.addListener(myFunction) // Attach listener function on state changes

let logoLists = document.querySelectorAll('.logo-list');

vertCarousel(logoLists[0]);
vertCarousel(logoLists[1]);
vertCarousel(logoLists[2]);
vertCarousel(logoLists[3]);
vertCarousel(logoLists[4]);

let slidesLg = document.querySelector('.testimonials .slideshow-lg');
let slidesSm = document.querySelector('.testimonials .slideshow-sm');

let horizCarouselLg = new horizCarousel(slidesLg, 0);
let horizCarouselSm = new horizCarousel(slidesSm, 0);

//Change slide in slideshow and slide out to correct side based
//on slide position

function clearAllSlideClasses (slideClassNum) {
    document.querySelector(slideClassNum).classList.remove('slideOutLeft');
    document.querySelector(slideClassNum).classList.remove('slideInRight');
    document.querySelector(slideClassNum).classList.remove('slideOutRight');
    document.querySelector(slideClassNum).classList.remove('slideInLeft');
    console.log('clearAllSlides helper function ran');
}

document.querySelector('.slide-right-to-1').addEventListener('click', function () {
    clearAllSlideClasses('.slide-1');
    clearAllSlideClasses('.slide-2');
    console.log('slide-right-to-1');
    clearInterval(horizCarouselLg.horizInterval);
    
    document.querySelector('.slide-2').classList.add('slideOutRight');
    document.querySelector('.slide-1').classList.add('slideInLeft');
    setTimeout(function () {
        document.querySelector('.slide-1').classList.remove('slideInLeft');
        console.log('manual slide timeout ended. carousel resuming...');
        clearInterval(horizCarouselLg.horizInterval);
        horizCarouselLg.restartInterval(1);
    }, 4000);
})

document.querySelector('.slide-left-to-2').addEventListener('click', function () {
    clearAllSlideClasses('.slide-2');
    clearAllSlideClasses('.slide-1');
    console.log('slide-right-to-2');
    clearInterval(horizCarouselLg.horizInterval);
    
    document.querySelector('.slide-1').classList.add('slideOutLeft');
    document.querySelector('.slide-2').classList.add('slideInRight');
    setTimeout(function () {
        document.querySelector('.slide-1').classList.remove('slideOutLeft');
        document.querySelector('.slide-2').classList.remove('slideInRight');
        console.log('manual slide timeout ended. carousel resuming...');
        clearInterval(horizCarouselLg.horizInterval);
        horizCarouselLg.restartInterval(0);
    }, 4000);
})

document.querySelector('.slide-right-to-3').addEventListener('click', function (el) {
    console.log('slide-right-to-3');
    clearAllSlideClasses('.slide-3');
    clearAllSlideClasses('.slide-4');
    clearInterval(horizCarouselSm.horizInterval);
    
    document.querySelector('.slide-4').classList.add('slideOutRight');
    document.querySelector('.slide-3').classList.add('slideInLeft');
    setTimeout(function () {
        document.querySelector('.slide-3').classList.remove('slideInLeft');
        console.log('manual slide timeout ended. carousel resuming...');
        clearInterval(horizCarouselSm.horizInterval);
        horizCarouselSm.restartInterval(1);
    }, 4000);
})

document.querySelector('.slide-left-to-4').addEventListener('click', function (el) {
    console.log('slide-left-to-4');
    clearAllSlideClasses('.slide-4');
    clearAllSlideClasses('.slide-3');
    clearInterval(horizCarouselSm.horizInterval);
    
    document.querySelector('.slide-3').classList.add('slideOutLeft');
    document.querySelector('.slide-4').classList.add('slideInRight');
    setTimeout(function () {
        document.querySelector('.slide-3').classList.remove('slideOutLeft');
        document.querySelector('.slide-4').classList.remove('slideInRight');
        document.querySelector('.slide-3').style.display = "none";
        console.log('manual slide timeout ended. carousel resuming...');
        clearInterval(horizCarouselSm.horizInterval);
        horizCarouselSm.restartInterval(0);
    }, 4000);
})