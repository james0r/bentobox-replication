export default class horizCarousel {
    constructor(el, initialPos) {
        this.children = el.children;
        this.prevElement;
        this.i = initialPos;
        this.horizInterval = setInterval(this.horizFunc.bind(this), 5000);
    }

    //showing initial image when page first loads
    setInitialPosition() {
        console.log("set initial position run");
        for (let k = 0; k < this.children.length; k++) {
            //iterate through children changing display none
            //for all except initial position
            if (k !== this.i) {
                this.children[k].style.display = "none";
            }
        }
    }

    testFunction() {
        console.log("the test function ran ");
    }

    restartInterval(initPos) {
        this.i = initPos;
        // this.setInitialPosition();
        
        this.i == this.children.length ? this.i = 0 : this.i;
        
        if (this.i == 0) {
            this.prevElement = this.children.length - 1;
        } else {
            this.prevElement = this.i - 1;
        }

        // this.children[this.prevElement].classList.remove('slideInRight');
        // this.children[this.i].classList.remove('slideOutLeft');

        this.horizInterval = setInterval(this.horizFunc.bind(this), 5000);
    }

    //Set element and initial position

    horizFunc() {
        console.log('horizFunc called');
        console.log(this.children);

        this.i == this.children.length ? this.i = 0 : this.i;

        if (this.i == 0) {
            this.prevElement = this.children.length - 1;
        } else {
            this.prevElement = this.i - 1;
        }
        this.children[this.prevElement].classList.remove('slideInRight');
        this.children[this.i].classList.remove('slideOutLeft');
        this.children[this.i].classList.remove('slideOutRight');
        

        this.children[this.prevElement].style.display = "block";
        this.children[this.i].style.display = "block";
        
        this.children[this.prevElement].classList.add('animated');
        this.children[this.prevElement].classList.add('slideOutLeft');
        this.children[this.i].classList.add('animated');
        this.children[this.i].classList.add('slideInRight');

        this.i++;
    }

}