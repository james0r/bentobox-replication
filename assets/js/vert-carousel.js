//vert-carousel takes an element as an argument
//and iterates through immediate children applying
//fadeOutUp to img leaving and fadeInUp to img entering

export default function (el) {
    let children = el.children;
    let prevElement;
    let i = 0;
    let j = 0;
    
    //showing initial image when page first loads
    for (let k = 1; k < children.length; k++) {
        children[k].style.display = "none";
    }

    setInterval(function () {
        for (j = 0; j < children.length; j++) {
            children[j].classList.remove('animated');
            children[j].classList.remove('fadeInUp');
            children[j].classList.remove('fadeOutUp');
            children[j].style.display = "none";
        }

        i == children.length ? i = 0 : i;

        if (i == 0) {
            prevElement = children.length -1;
        } else {
            prevElement = i - 1;
        }
        children[prevElement].style.display = "block";
        children[i].style.display = "block";
        children[prevElement].classList.add('animated');
        children[prevElement].classList.add('fadeOutUp');
        children[i].classList.add('animated');
        children[i].classList.add('fadeInUp');

        i++;
    }, 4000)
}